```txt
  
  ____             _                
 |  _ \ __ _  ___ | | ___           
 | |_) / _` |/ _ \| |/ _ \          
 |  __/ (_| | (_) | | (_) |         
 |_|  _\__,_|\___/|_|\___/      _   
 |  \/  | ___  _ __(_)___  ___ | |_ 
 | |\/| |/ _ \| '__| / __|/ _ \| __|
 | |  | | (_) | |  | \__ \ (_) | |_ 
 |_|  |_|\___/|_|  |_|___/\___/ \__|
```

# Hunt Search
This app is designed to test the properties of ProductHunt API.

## Requirement
To run the app you'll need two things:

-  docker
- docker-compose 

Refer to proper documentation to install these on your system.

## Env setting
There is some environment variables you need to set in order to run this project.

### For client
Go in ./client directory and create a .env file with the following configuration
GRIDSOME_API_URL = http://localhost:3000

### For API
Go in ./nest-api directory and create a .env file with the following configuration:

PRODUCT_HUNT_API_KEY= {your_api_key_goes_here}
PRODUCT_HUNT_API_SECRET= {your_api_secret_goes_here}

To learn more about product hunt api you can follow this link:  https://api.producthunt.com/v2/docs/oauth_client_only_authentication/oauth_token_ask_for_client_level_token 

## Run the app 
To run the app simply run compose file at root level:
```
docker-compose up -d
```
## API development
Backend is developed in TS using nest.js framework.
You can check ./nest-app/README.md to see available commands for dev and tests.

To start working on the app, simply go in ./nest-app directory and run
```
npm run start:dev
```

## Front development
Frontend is developed using gridsome (Vue.js static framework).
It's recommended to install the CLI to use it:
```
npm install -g gridsome
```
You can start hacking by going in the ./client directory and run 
```
gridsome develop
```
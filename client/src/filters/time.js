import Vue from 'vue'
// Get french date from a js formatted date
const dateFrench = date => {
  const options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric'
  }
  const dateObject = new Date(date)
  return dateObject.toLocaleDateString('fr-FR', options)
}
Vue.filter('dateFrench', dateFrench)

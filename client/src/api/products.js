import request from '../utils/request'
const ENDPOINT = '/product-hunt'

/**
 * fetch products with custom parameters
 * @param params
 * @returns {*}
 */
export const fetchAll = async params => {
  let queryString = ''
  // Add first
  let urlOperator = queryString === '' ? '?' : '&'
  if (params.first) {
    queryString = `${queryString}${urlOperator}filter[first]=${params.first}`
  }
  // Add after
  urlOperator = queryString === '' ? '?' : '&'
  if (typeof params.after !== 'undefined') {
    queryString = `${queryString}${urlOperator}filter[after]=${params.after}`
  }
  // Add postedBefore
  urlOperator = queryString === '' ? '?' : '&'
  if (params.postedBefore) {
    queryString = `${queryString}${urlOperator}filter[postedBefore]=${params.postedBefore}`
  }
  return request({
    url: `${ENDPOINT}/${queryString}`,
    method: 'get'
  })
}

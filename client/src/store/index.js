import Vue from 'vue'
import Vuex from 'vuex'
import producthunt from './modules/producthunt'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    producthunt
  }
})

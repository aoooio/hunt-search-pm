import { fetchAll } from '../../api/products'

const state = {
  products: [],
  productsLoading: false,
  productsError: false,
  postedBefore: '',
  lastCursor: ''
}

// mutations
const mutations = {
  FETCH_PRODUCTS: state => {
    state.productLoading = true
  },
  FETCH_PRODUCTS_SUCCESS: (state, products) => {
    state.productLoading = false
    state.products = products
    state.currentItemError = false
  },
  FETCH_PRODUCTS_ERROR: (state, error) => {
    state.productLoading = false
    state.currentItemError = error
  },
  ADD_PRODUCTS: (state, products) => {
    products.map(product => {
      state.products.push(product)
    })
  },
  SET_LAST_CURSOR: (state, lastCursor) => {
    state.lastCursor = lastCursor
  },
  SET_POSTED_BEFORE: (state, lastCursor) => {
    state.postedBefore = lastCursor
  }
}

// actions
const actions = {
  async fetchProducts({ commit, state }, postedBefore) {
    commit('FETCH_PRODUCTS')
    try {
      const result = await fetchAll({ postedBefore })
      const productsFormatted = result.data.data.posts.edges.map(item => {
        return { ...item.node, cursor: item.cursor }
      })
      const lastCursor = productsFormatted[productsFormatted.length - 1].cursor
      commit('FETCH_PRODUCTS_SUCCESS', productsFormatted)
      commit('SET_LAST_CURSOR', lastCursor)
      commit('SET_POSTED_BEFORE', postedBefore)
      return Promise.resolve(result)
    } catch (e) {
      commit('FETCH_PRODUCTS_ERROR', e)
      return Promise.reject(e)
    }
  },
  async loadMoreProduct({ commit, state }) {
    commit('FETCH_PRODUCTS')
    try {
      const result = await fetchAll({
        after: state.lastCursor,
        postedBefore: state.postedBefore
      })
      const productsFormatted = result.data.data.posts.edges.map(item => {
        return { ...item.node, cursor: item.cursor }
      })
      const lastCursor = productsFormatted[productsFormatted.length - 1].cursor
      commit('ADD_PRODUCTS', productsFormatted)
      commit('SET_LAST_CURSOR', lastCursor)
      return Promise.resolve(result)
    } catch (e) {
      commit('FETCH_PRODUCTS_ERROR', e)
      return Promise.reject(e)
    }
  }
}

// Getters
const getters = {}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}

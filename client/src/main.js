// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from '~/layouts/Default.vue'

// ===================
// Vuetify UI lib
// ===================
// Add vuetify
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
// Add mdi icons
import '@mdi/font/css/materialdesignicons.css'

// ===================
// Import filters globally
// ===================
import './filters/time'

// ===================
// Store Vuex
// ===================
import store from './store'

export default function(Vue, { appOptions, router, head }) {
  // Add responsive meta to head
  head.meta.push({
    name: 'viewport',
    content: 'width=device-width, initial-scale=1'
  })
  // Add vuetify headers
  head.link.push({
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/icon?family=Material+Icons'
  })
  const opts = {
    icons: {
      iconfont: 'mdi' // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4'
    },
    theme: {
      themes: {
        light: {
          primary: '#6d00fd',
          secondary: '#b0bec5',
          accent: '#8c9eff',
          error: '#b71c1c'
        }
      }
    }
  }

  //opts includes, vuetify themes, icons, etc.
  Vue.use(Vuetify)
  appOptions.vuetify = new Vuetify(opts)

  // Add store to app options
  appOptions.store = store
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)
}

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductHuntModule } from './product-hunt/product-hunt.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    ProductHuntModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

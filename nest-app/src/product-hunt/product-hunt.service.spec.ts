import { Test, TestingModule } from '@nestjs/testing';
import { ProductHuntService } from './product-hunt.service';

describe('ProductHuntService', () => {
  let service: ProductHuntService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProductHuntService],
    }).compile();

    service = module.get<ProductHuntService>(ProductHuntService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

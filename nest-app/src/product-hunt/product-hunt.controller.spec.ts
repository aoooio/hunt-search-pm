import { Test, TestingModule } from '@nestjs/testing';
import { ProductHuntController } from './product-hunt.controller';

describe('ProductHuntController', () => {
  let controller: ProductHuntController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductHuntController],
    }).compile();

    controller = module.get<ProductHuntController>(ProductHuntController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

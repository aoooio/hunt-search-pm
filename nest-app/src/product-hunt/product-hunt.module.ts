import { Module } from '@nestjs/common';
import { ProductHuntService } from './product-hunt.service';
import { ProductHuntController } from './product-hunt.controller';

@Module({
  providers: [ProductHuntService],
  controllers: [ProductHuntController]
})
export class ProductHuntModule {}

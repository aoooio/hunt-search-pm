import { Injectable } from '@nestjs/common';
import axios from 'axios';
const PRODUCT_HUNT_URI = `https://api.producthunt.com`;

@Injectable()
export class ProductHuntService {
  /**
   * get Authorization header for ProductHunt Api
   * Warning: Auth is based on envs PRODUCT_HUNT_API_KEY and PRODUCT_HUNT_API_SECRET
   * @see: https://api.producthunt.com/v2/docs/oauth_client_only_authentication/oauth_token_ask_for_client_level_token
   * @private
   */
  private async getProductHuntAuthHeader(): Promise<string> {
    const authResponse = await axios.post(
      `${PRODUCT_HUNT_URI}/v2/oauth/token/`,
      {
        client_id: process.env.PRODUCT_HUNT_API_KEY,
        client_secret: process.env.PRODUCT_HUNT_API_SECRET,
        grant_type: 'client_credentials',
      },
    );

    return `Bearer ${authResponse.data.access_token}`;
  }

  /**
   * Find "Producthunt" products by date
   * @param first
   * @param after
   * @param postedBefore
   */
  public async findProductsByDate(
    first = 10,
    after: string | undefined,
    postedBefore: Date | undefined,
  ): Promise<any> {
    try {
      // Prepare Authorization headers
      const AuthHeader = await this.getProductHuntAuthHeader();

      // Prepare uery parameters
      const afterQueryParam = after ? `after: "${after}"` : '';
      const postedBeforeQueryParam = postedBefore
        ? `postedBefore: "${postedBefore}"`
        : '';

      // TODO for real usecase Sanitize params
      // Prepare graphQL Query
      const graphQlQuery = `
        {
          posts(
            first: ${first}
            order: NEWEST
            ${afterQueryParam}
            ${postedBeforeQueryParam}
          ) {
            edges {
              node {
                id
                name
                description
                featuredAt
                createdAt
                reviewsRating
                reviewsCount
                website
                user {
                  username
                  id
                }
                thumbnail {
                  type
                  url
                }
                
              }
              cursor
            }
          }
        }      
    `;

      // Call Product Hunt API
      return axios.post(
        `${PRODUCT_HUNT_URI}/v2/api/graphql`,
        {
          query: graphQlQuery,
        },
        {
          headers: {
            Authorization: AuthHeader,
          },
        },
      );
    } catch (e) {
      // TODO for real usecases handle status code
      return {
        error:
          'Something went wrong during auth or during productHunt API call',
      };
    }
  }
}

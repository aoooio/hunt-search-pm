import {
  Controller,
  HttpStatus,
  Response,
  Request,
  Get,
  Query,
} from '@nestjs/common';
import { ProductHuntService } from './product-hunt.service';

@Controller('product-hunt')
export class ProductHuntController {
  constructor(private readonly productHuntService: ProductHuntService) {}
  @Get('')
  async findProducts(@Response() res, @Query() query) {
    const defaultFirst = 10;
    const defaultAfter = undefined;
    const defaultPostedBefore = undefined;
    let first, after, postedBefore;

    // Check if filters are defined
    if (query.filter) {
      first = query.filter.first ? query.filter.first : defaultFirst;
      after = query.filter.after ? query.filter.after : defaultAfter;
      postedBefore = query.filter.postedBefore
        ? query.filter.postedBefore
        : defaultPostedBefore;
    }

    const { data } = await this.productHuntService.findProductsByDate(
      first,
      after,
      postedBefore,
    );

    return res.json(data);
  }
}
